const url = "http://localhost:8000/api/locations/";

document.addEventListener("DOMContentLoaded", async () => {
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    const locationSelect = document.getElementById("location");
    for (const { id, name } of data.locations) {
      locationSelect.innerHTML += `<option value="${id}">${name}</option>`;
    }
  } else {
    console.log("Error");
  }

  const form = document.getElementById("create-conference-form");
  form.addEventListener("submit", async (event) => {
    event.preventDefault();

    const formData = new FormData(form);

    const data = Object.fromEntries(formData);
    const conferencesUrl = "http://localhost:8000/api/conferences/";
    const response = await fetch(conferencesUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      form.reset();
      const newConference = await response.json();
      console.log(newConference);
    } else {
      console.log("Error submitting form");
    }
  });
});
